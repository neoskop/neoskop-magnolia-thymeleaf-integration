# README

Allgemeine Beschreibung vom Modul **Neoskop Magnolia Thymeleaf Integration**.

# Abhängigkeiten

- [Magnolia CMS][1] >= 5.4.6
- [Magnolia Blossom][4] >= 3.1.3

# Installation

Das Modul muss in der `pom.xml` des Magnolia-Projektes als Abhängigkeit hinzugefügt werden:

```xml
<dependency>
	<groupId>org.bitbucket.neoskop</groupId>
	<artifactId>neoskop-magnolia-thymeleaf-integration</artifactId>
	<version>1.7.1</version>
</dependency>
```

# Verwendung des Moduls

Beispiel YAML/JCR-Template:

```yaml
pages:
  poolComponentPage:
    visible: true
    renderType: thymeleaf
    templateScript: /hannoversche-website-components/templates/pages/componentPage.html
```

Spring/Blossom-Konfiguration:

```java
@Configuration
public class ThymeleafRenderingConfiguration {

	private ApplicationContext applicationContext;

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Bean
	public TemplateViewResolver thymeleafTemplateViewResolver() {
		TemplateViewResolver resolver = new TemplateViewResolver();
		resolver.setOrder(3);
		resolver.setPrefix(StringUtils.EMPTY);
		resolver.setViewNames("*.html");
		resolver.setViewRenderer(thymeleafViewRenderer());

		return resolver;
	}

	@Bean
	public ThymeleafBlossomRenderer thymeleafViewRenderer(){
		ThymeleafBlossomRenderer viewRenderer = new ThymeleafBlossomRenderer();
		viewRenderer.setEngine(templateEngine());

		viewRenderer.addContextAttribute("cmsfn", TemplatingFunctions.class);
		viewRenderer.addContextAttribute("damfn", DamTemplatingFunctions.class);

		return viewRenderer;
	}

	@Bean
	public SpringTemplateEngine templateEngine() {
		SpringTemplateEngine engine = new SpringTemplateEngine();
		engine.setEnableSpringELCompiler(true);
		engine.setTemplateResolver(templateResolver());
		engine.addDialect(new MagnoliaDialect());

		return engine;
	}

	@Bean
	public ITemplateResolver templateResolver() {
		SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
		resolver.setApplicationContext(applicationContext);
		resolver.setPrefix("/templates/hannoversche-website-templates/");
		resolver.setSuffix(StringUtils.EMPTY);
		resolver.setTemplateMode(TemplateMode.HTML);
		resolver.setCacheable(false);

		return resolver;
	}
}
```

[1]: https://www.magnolia-cms.com
[2]: http://maven.neoskop.io
[3]: http://maven.apache.org/maven-release/maven-release-plugin/
[4]: https://documentation.magnolia-cms.com/display/DOCS/Blossom+module
