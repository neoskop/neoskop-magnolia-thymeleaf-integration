package de.neoskop.magnolia.thymeleaf.templateresource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import org.apache.commons.lang3.StringUtils;
import org.thymeleaf.templateresource.ClassLoaderTemplateResource;
import org.thymeleaf.templateresource.FileTemplateResource;
import org.thymeleaf.templateresource.ITemplateResource;
import org.thymeleaf.util.Validate;

/** Created by cbinzer on 14.03.17. */
public class FileAndClassLoaderTemplateResource implements ITemplateResource, Serializable {

  private ClassLoader classLoader;
  private String path;
  private String characterEncoding;
  private String prefix;

  private FileTemplateResource fileTemplateResource;
  private ClassLoaderTemplateResource classLoaderTemplateResource;

  public FileAndClassLoaderTemplateResource(
      final ClassLoader classLoader,
      final String path,
      final String characterEncoding,
      final String prefix) {

    String classLoaderPath = path.replace(prefix, StringUtils.EMPTY);

    this.classLoader = classLoader;
    this.path = path;
    this.characterEncoding = characterEncoding;
    this.prefix = prefix;

    this.fileTemplateResource = new FileTemplateResource(path, characterEncoding);
    this.classLoaderTemplateResource =
        new ClassLoaderTemplateResource(classLoader, classLoaderPath, characterEncoding);
  }

  @Override
  public String getDescription() {
    return path;
  }

  @Override
  public String getBaseName() {
    return classLoaderTemplateResource.getBaseName();
  }

  @Override
  public boolean exists() {
    return fileTemplateResource.exists() || classLoaderTemplateResource.exists();
  }

  @Override
  public Reader reader() throws IOException {
    try {
      return fileTemplateResource.reader();
    } catch (FileNotFoundException e) {
      return classLoaderTemplateResource.reader();
    }
  }

  @Override
  public ITemplateResource relative(String relativeLocation) {
    Validate.notEmpty(relativeLocation, "Relative Path cannot be null or empty");

    final String fullRelativeLocation = computeRelativeLocation(this.path, relativeLocation);
    return new FileAndClassLoaderTemplateResource(
        this.classLoader, fullRelativeLocation, this.characterEncoding, this.prefix);
  }

  private String computeRelativeLocation(final String location, final String relativeLocation) {
    final int separatorPos = location.lastIndexOf('/');
    if (separatorPos != -1) {
      final StringBuilder relativeBuilder =
          new StringBuilder(location.length() + relativeLocation.length());
      relativeBuilder.append(location, 0, separatorPos);
      if (relativeLocation.charAt(0) != '/') {
        relativeBuilder.append('/');
      }
      relativeBuilder.append(relativeLocation);
      return relativeBuilder.toString();
    }
    return relativeLocation;
  }
}
