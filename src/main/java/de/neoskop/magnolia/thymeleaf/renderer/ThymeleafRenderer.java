package de.neoskop.magnolia.thymeleaf.renderer;

import de.neoskop.magnolia.thymeleaf.util.AppendableWriterWrapper;
import info.magnolia.context.MgnlContext;
import info.magnolia.context.WebContext;
import info.magnolia.rendering.context.RenderingContext;
import info.magnolia.rendering.engine.RenderException;
import info.magnolia.rendering.engine.RenderingEngine;
import info.magnolia.rendering.renderer.AbstractRenderer;
import info.magnolia.rendering.template.RenderableDefinition;
import info.magnolia.rendering.util.AppendableWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import javax.jcr.Node;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.IContext;

public class ThymeleafRenderer extends AbstractRenderer {

  private static final String SEPARATOR = "::";

  private TemplateEngine templateEngine;

  @Inject
  public ThymeleafRenderer(RenderingEngine renderingEngine) {
    super(renderingEngine);
  }

  @Override
  protected void onRender(
      Node content,
      RenderableDefinition definition,
      RenderingContext renderingCtx,
      Map<String, Object> ctx,
      String templateScript)
      throws RenderException {

    Set<String> selectors = new HashSet<>();
    if (templateScript.contains(SEPARATOR)) {
      String[] split = templateScript.split(SEPARATOR);
      templateScript = split[0].trim();
      selectors.add(split[1].trim());
    }

    try {
      if (MgnlContext.hasInstance()) {
        ctx.put("ctx", MgnlContext.getInstance());
      }

      if (MgnlContext.isWebContext()) {
        WebContext mgnlWebContext = MgnlContext.getWebContext();
        ctx.put("request", mgnlWebContext.getRequest());
        ctx.put("response", mgnlWebContext.getResponse());
      }

      AppendableWriter out = renderingCtx.getAppendable();
      IContext context = new Context(MgnlContext.getAggregationState().getLocale(), ctx);

      templateEngine.process(templateScript, selectors, context, new AppendableWriterWrapper(out));
    } catch (IOException e) {
      throw new RenderException(e);
    }
  }

  @Override
  protected Map<String, Object> newContext() {
    return new HashMap<>();
  }

  public TemplateEngine getTemplateEngine() {
    return templateEngine;
  }

  public void setTemplateEngine(TemplateEngine templateEngine) {
    this.templateEngine = templateEngine;
  }
}
