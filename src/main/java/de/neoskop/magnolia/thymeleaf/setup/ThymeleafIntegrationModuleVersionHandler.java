package de.neoskop.magnolia.thymeleaf.setup;

import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.delta.BootstrapSingleResource;
import info.magnolia.module.delta.DeltaBuilder;

/**
 * This class is optional and lets you manage the versions of your module, by registering "deltas"
 * to maintain the module's configuration, or other type of content. If you don't need this, simply
 * remove the reference to this class in the module descriptor xml.
 *
 * @see info.magnolia.module.DefaultModuleVersionHandler
 * @see info.magnolia.module.ModuleVersionHandler
 * @see info.magnolia.module.delta.Task
 */
public class ThymeleafIntegrationModuleVersionHandler extends DefaultModuleVersionHandler {

  public ThymeleafIntegrationModuleVersionHandler() {
    register(
        DeltaBuilder.update("1.6.1", "")
            .addTask(
                new BootstrapSingleResource(
                    "",
                    "",
                    "/mgnl-bootstrap/update-1.6.1/config.modules.rendering.renderers.thymeleaf.templateEngine.messageResolvers.xml")));
  }
}
