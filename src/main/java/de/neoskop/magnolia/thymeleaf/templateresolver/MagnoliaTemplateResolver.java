package de.neoskop.magnolia.thymeleaf.templateresolver;

import de.neoskop.magnolia.thymeleaf.templateresource.FileAndClassLoaderTemplateResource;
import info.magnolia.init.MagnoliaConfigurationProperties;
import java.util.Map;
import javax.inject.Inject;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.templateresolver.AbstractConfigurableTemplateResolver;
import org.thymeleaf.templateresource.ClassLoaderTemplateResource;
import org.thymeleaf.templateresource.ITemplateResource;
import org.thymeleaf.util.ClassLoaderUtils;

/** Created by cbinzer on 13.09.16. */
public class MagnoliaTemplateResolver extends AbstractConfigurableTemplateResolver {

  private ClassLoader classLoader;
  private MagnoliaConfigurationProperties magnoliaConfigurationProperties;

  @Inject
  public MagnoliaTemplateResolver(MagnoliaConfigurationProperties magnoliaConfigurationProperties) {
    this.classLoader = ClassLoaderUtils.getClassLoader(MagnoliaTemplateResolver.class);
    this.magnoliaConfigurationProperties = magnoliaConfigurationProperties;

    if (magnoliaConfigurationProperties.getBooleanProperty("magnolia.develop")) {
      setPrefix(magnoliaConfigurationProperties.getProperty("magnolia.resources.dir"));
    }
  }

  @Override
  protected ITemplateResource computeTemplateResource(
      IEngineConfiguration configuration,
      String ownerTemplate,
      String template,
      String resourceName,
      String characterEncoding,
      Map<String, Object> templateResolutionAttributes) {

    if (magnoliaConfigurationProperties.getBooleanProperty("magnolia.develop")) {
      return new FileAndClassLoaderTemplateResource(
          classLoader, resourceName, characterEncoding, getPrefix());
    } else {
      return new ClassLoaderTemplateResource(classLoader, resourceName, characterEncoding);
    }
  }
}
