package de.neoskop.magnolia.thymeleaf.dialect;

import de.neoskop.magnolia.thymeleaf.processor.CmsAreaElementProcessor;
import de.neoskop.magnolia.thymeleaf.processor.CmsComponentElementProcessor;
import de.neoskop.magnolia.thymeleaf.processor.CmsInitElementProcessor;
import de.neoskop.magnolia.thymeleaf.processor.CmsSrcTagProcessor;
import de.neoskop.magnolia.thymeleaf.processor.CmsStyleTagProcessor;
import de.neoskop.magnolia.thymeleaf.processor.CmsTextTagProcessor;
import de.neoskop.magnolia.thymeleaf.processor.CmsUtextTagProcessor;
import de.neoskop.magnolia.thymeleaf.processor.CmsVBindTagProcessor;
import java.util.HashSet;
import java.util.Set;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;

public class MagnoliaDialect extends AbstractProcessorDialect {

  public static final String NAME = "Magnolia";
  public static final String PREFIX = "cms";
  public static final int PROCESSOR_PRECEDENCE = 101;

  public MagnoliaDialect() {
    super(NAME, PREFIX, PROCESSOR_PRECEDENCE);
  }

  public Set<IProcessor> getProcessors(String dialectName) {
    Set<IProcessor> processors = new HashSet<>();
    processors.add(new CmsInitElementProcessor(dialectName));
    processors.add(new CmsAreaElementProcessor(dialectName));
    processors.add(new CmsComponentElementProcessor(dialectName));
    processors.add(new CmsTextTagProcessor(dialectName));
    processors.add(new CmsTextTagProcessor(dialectName));
    processors.add(new CmsUtextTagProcessor(dialectName));
    processors.add(new CmsSrcTagProcessor(dialectName));
    processors.add(new CmsStyleTagProcessor(dialectName));
    processors.add(new CmsVBindTagProcessor(dialectName));

    return processors;
  }
}
