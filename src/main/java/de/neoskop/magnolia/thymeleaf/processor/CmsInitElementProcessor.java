package de.neoskop.magnolia.thymeleaf.processor;

import info.magnolia.cms.beans.config.ServerConfiguration;
import info.magnolia.cms.core.AggregationState;
import info.magnolia.context.MgnlContext;
import info.magnolia.objectfactory.Components;
import info.magnolia.rendering.context.RenderingContext;
import info.magnolia.rendering.engine.RenderingEngine;
import info.magnolia.rendering.template.TemplateDefinition;
import info.magnolia.templating.elements.MarkupHelper;
import info.magnolia.templating.freemarker.AbstractDirective;
import java.io.IOException;
import java.io.StringWriter;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.exceptions.TemplateProcessingException;
import org.thymeleaf.model.IComment;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.processor.element.AbstractAttributeModelProcessor;
import org.thymeleaf.processor.element.IElementModelStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

public class CmsInitElementProcessor extends AbstractAttributeModelProcessor {

  public static final String ATTRIBUTE_NAME = "init";

  private RenderingEngine renderingEngine;
  private ServerConfiguration serverConfiguration;

  public CmsInitElementProcessor(String prefix) {
    super(TemplateMode.HTML, prefix, "head", false, ATTRIBUTE_NAME, true, 1000, true);

    this.renderingEngine = Components.getComponent(RenderingEngine.class);
    this.serverConfiguration = Components.getComponent(ServerConfiguration.class);
  }

  @Override
  protected void doProcess(
      ITemplateContext context,
      IModel model,
      AttributeName attributeName,
      String attributeValue,
      IElementModelStructureHandler structureHandler) {

    if (!serverConfiguration.isAdmin()) {
      return;
    }

    AggregationState aggregationState = MgnlContext.getAggregationState();
    Node activePage = aggregationState.getMainContentNode();

    TemplateMode templateMode = context.getTemplateMode();
    IModelFactory modelFactory = context.getConfiguration().getModelFactory(templateMode);

    StringWriter writer = new StringWriter();
    MarkupHelper helper = new MarkupHelper(writer);

    try {
      helper.append(" cms:page");
      if (activePage != null) {
        helper.attribute(AbstractDirective.CONTENT_ATTRIBUTE, getNodePath(activePage));
      }

      RenderingContext renderingContext = renderingEngine.getRenderingContext();
      TemplateDefinition templateDefinition =
          (TemplateDefinition) renderingContext.getRenderableDefinition();
      String dlg = templateDefinition.getDialog();

      if (dlg != null) {
        helper.attribute("dialog", dlg);
      }

      helper.append(" ");
    } catch (IOException e) {
      throw new TemplateProcessingException("An error occurred on creating cms init comment", e);
    }

    IComment comment = modelFactory.createComment(writer.toString());
    model.insert(model.size() - 1, comment);
  }

  protected String getNodePath(Node node) throws TemplateProcessingException {
    try {
      return node.getSession().getWorkspace().getName() + ":" + node.getPath();
    } catch (RepositoryException e) {
      throw new TemplateProcessingException("Can't construct node path for node " + node, e);
    }
  }
}
