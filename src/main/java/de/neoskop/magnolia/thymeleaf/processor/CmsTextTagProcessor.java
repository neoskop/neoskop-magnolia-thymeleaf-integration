package de.neoskop.magnolia.thymeleaf.processor;

import org.apache.commons.lang3.StringUtils;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.exceptions.TemplateProcessingException;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.processor.AbstractStandardExpressionAttributeTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.util.LazyEscapingCharSequence;
import org.unbescape.html.HtmlEscape;
import org.unbescape.xml.XmlEscape;

/** Created by cbinzer on 29.06.16. */
public class CmsTextTagProcessor extends AbstractStandardExpressionAttributeTagProcessor {

  public static final int PRECEDENCE = 1300;
  public static final String ATTR_NAME = "text";

  public CmsTextTagProcessor(String dialectPrefix) {
    super(TemplateMode.HTML, dialectPrefix, ATTR_NAME, PRECEDENCE, true);
  }

  private static String produceEscapedOutput(final TemplateMode templateMode, final String input) {
    switch (templateMode) {
      case TEXT:
        // fall-through
      case HTML:
        return HtmlEscape.escapeHtml4Xml(input);
      case XML:
        return XmlEscape.escapeXml10(input);
      default:
        throw new TemplateProcessingException(
            "Unrecognized template mode "
                + templateMode
                + ". Cannot produce escaped output for "
                + "this template mode.");
    }
  }

  @Override
  protected void doProcess(
      ITemplateContext context,
      IProcessableElementTag tag,
      AttributeName attributeName,
      String attributeValue,
      Object expressionResult,
      IElementTagStructureHandler structureHandler) {

    final TemplateMode templateMode = getTemplateMode();
    final CharSequence text;

    if (templateMode != TemplateMode.JAVASCRIPT && templateMode != TemplateMode.CSS) {
      final String input = (expressionResult == null ? "" : expressionResult.toString());

      if (templateMode == TemplateMode.RAW) {
        text = input;
      } else {
        if (input.length() > 100) {
          text = new LazyEscapingCharSequence(context.getConfiguration(), templateMode, input);
        } else {
          text = produceEscapedOutput(templateMode, input);
        }
      }
    } else {
      text =
          new LazyEscapingCharSequence(context.getConfiguration(), templateMode, expressionResult);
    }

    if (StringUtils.isNotBlank(text)) {
      structureHandler.setBody(text, false);
    }
  }
}
