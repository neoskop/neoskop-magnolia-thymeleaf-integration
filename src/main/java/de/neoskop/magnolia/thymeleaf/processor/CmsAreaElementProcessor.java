package de.neoskop.magnolia.thymeleaf.processor;

import info.magnolia.objectfactory.Components;
import info.magnolia.rendering.context.RenderingContext;
import info.magnolia.rendering.engine.RenderingEngine;
import info.magnolia.rendering.template.AreaDefinition;
import info.magnolia.rendering.template.configured.ConfiguredTemplateDefinition;
import info.magnolia.templating.elements.AreaElement;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.exceptions.TemplateProcessingException;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

public class CmsAreaElementProcessor extends AbstractCmsElementProcessor<AreaElement> {

  public static final String ATTRIBUTE_NAME = "area";

  public CmsAreaElementProcessor(String prefix) {
    super(TemplateMode.HTML, prefix, null, false, ATTRIBUTE_NAME, true);
  }

  @Override
  protected void doProcess(
      ITemplateContext context,
      IProcessableElementTag tag,
      AttributeName attributeName,
      String attributeValue,
      IElementTagStructureHandler structureHandler) {

    RenderingEngine renderingEngine = Components.getComponent(RenderingEngine.class);
    RenderingContext renderingContext = renderingEngine.getRenderingContext();
    ConfiguredTemplateDefinition templateDefinition =
        (ConfiguredTemplateDefinition) renderingContext.getRenderableDefinition();
    AreaDefinition areaDefinition = templateDefinition.getAreas().get(attributeValue);

    if (areaDefinition == null) {
      throw new TemplateProcessingException("Area not found: " + attributeValue);
    }

    AreaElement areaElement = createElement(renderingContext);
    areaElement.setName(areaDefinition.getName());
    areaElement.setEditable(getEditable(tag));
    areaElement.setContextAttributes(getContextAttributes(context, tag));

    processElement(structureHandler, areaElement);
  }
}
