package de.neoskop.magnolia.thymeleaf.processor;

import info.magnolia.jcr.util.ContentMap;
import info.magnolia.objectfactory.Components;
import info.magnolia.rendering.context.RenderingContext;
import info.magnolia.rendering.engine.RenderingEngine;
import info.magnolia.templating.elements.ComponentElement;
import javax.jcr.Node;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.exceptions.TemplateProcessingException;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.expression.Expression;
import org.thymeleaf.standard.expression.StandardExpressionParser;
import org.thymeleaf.templatemode.TemplateMode;

public class CmsComponentElementProcessor extends AbstractCmsElementProcessor<ComponentElement> {

  public static final String ATTRIBUTE_NAME = "component";

  public CmsComponentElementProcessor(String prefix) {
    super(TemplateMode.HTML, prefix, null, false, ATTRIBUTE_NAME, true);
  }

  @Override
  protected void doProcess(
      ITemplateContext context,
      IProcessableElementTag tag,
      AttributeName attributeName,
      String attributeValue,
      IElementTagStructureHandler structureHandler) {

    Expression expression = new StandardExpressionParser().parseExpression(context, attributeValue);
    Object contentObject = expression.execute(context);
    Node content;

    if (contentObject instanceof ContentMap) {
      content = ((ContentMap) contentObject).getJCRNode();
    } else if (contentObject instanceof Node) {
      content = (Node) contentObject;
    } else {
      throw new TemplateProcessingException(
          "Cannot cast " + contentObject.getClass() + " to javax.jcr.Node");
    }

    RenderingEngine renderingEngine = Components.getComponent(RenderingEngine.class);
    RenderingContext renderingContext = renderingEngine.getRenderingContext();

    ComponentElement componentElement = createElement(renderingContext);
    componentElement.setContent(content);
    componentElement.setEditable(getEditable(tag));
    componentElement.setContextAttributes(getContextAttributes(context, tag));

    processElement(structureHandler, componentElement);
  }
}
