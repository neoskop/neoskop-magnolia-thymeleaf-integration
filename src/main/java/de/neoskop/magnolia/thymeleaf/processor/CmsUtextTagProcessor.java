package de.neoskop.magnolia.thymeleaf.processor;

import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.engine.TemplateModel;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.postprocessor.IPostProcessor;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.expression.Fragment;
import org.thymeleaf.standard.expression.FragmentExpression;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.NoOpToken;
import org.thymeleaf.standard.expression.StandardExpressionExecutionContext;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.templatemode.TemplateMode;

/** Created by cbinzer on 30.06.16. */
public class CmsUtextTagProcessor extends AbstractAttributeTagProcessor {

  public static final int PRECEDENCE = 1400;
  public static final String ATTR_NAME = "utext";

  public CmsUtextTagProcessor(final String dialectPrefix) {
    super(TemplateMode.HTML, dialectPrefix, null, false, ATTR_NAME, true, PRECEDENCE, true);
  }

  private static boolean mightContainStructures(final CharSequence unescapedText) {
    int n = unescapedText.length();
    char c;
    while (n-- != 0) {
      c = unescapedText.charAt(n);
      if (c == '>' || c == ']') {
        return true;
      }
    }
    return false;
  }

  @Override
  protected void doProcess(
      final ITemplateContext context,
      final IProcessableElementTag tag,
      final AttributeName attributeName,
      final String attributeValue,
      final IElementTagStructureHandler structureHandler) {

    final IEngineConfiguration configuration = context.getConfiguration();
    final IStandardExpressionParser expressionParser =
        StandardExpressions.getExpressionParser(configuration);
    final IStandardExpression expression =
        expressionParser.parseExpression(context, attributeValue);

    final Object expressionResult;
    if (expression != null && expression instanceof FragmentExpression) {
      final FragmentExpression.ExecutedFragmentExpression executedFragmentExpression =
          FragmentExpression.createExecutedFragmentExpression(
              context,
              (FragmentExpression) expression,
              StandardExpressionExecutionContext.RESTRICTED);
      expressionResult =
          FragmentExpression.resolveExecutedFragmentExpression(
              context, executedFragmentExpression, true);
    } else {
      assert expression != null;
      expressionResult = expression.execute(context, StandardExpressionExecutionContext.RESTRICTED);
    }

    // If result is no-op, there's nothing to execute
    if (expressionResult == NoOpToken.VALUE) {
      return;
    }

    if (expressionResult != null && expressionResult instanceof Fragment) {
      if (expressionResult == Fragment.EMPTY_FRAGMENT) {
        structureHandler.removeBody();
        return;
      }

      structureHandler.setBody(((Fragment) expressionResult).getTemplateModel(), false);

      return;
    }

    final String unescapedTextStr = (expressionResult == null ? "" : expressionResult.toString());
    final Set<IPostProcessor> postProcessors = configuration.getPostProcessors(getTemplateMode());

    if (postProcessors.isEmpty()) {
      if (StringUtils.isNotBlank(unescapedTextStr)) {
        structureHandler.setBody(unescapedTextStr, false);
      }

      return;
    }

    if (!mightContainStructures(unescapedTextStr)) {
      if (StringUtils.isNotBlank(unescapedTextStr)) {
        structureHandler.setBody(unescapedTextStr, false);
      }

      return;
    }

    final TemplateModel parsedFragment =
        configuration
            .getTemplateManager()
            .parseString(context.getTemplateData(), unescapedTextStr, 0, 0, null, false);

    structureHandler.setBody(parsedFragment, false);
  }
}
