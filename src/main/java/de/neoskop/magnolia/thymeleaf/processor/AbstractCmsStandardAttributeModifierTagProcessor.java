package de.neoskop.magnolia.thymeleaf.processor;

import org.apache.commons.lang3.StringUtils;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeDefinition;
import org.thymeleaf.engine.AttributeDefinitions;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.engine.IAttributeDefinitionsAware;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.processor.AbstractStandardExpressionAttributeTagProcessor;
import org.thymeleaf.standard.util.StandardProcessorUtils;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.util.Validate;
import org.unbescape.html.HtmlEscape;

/** Created by cbinzer on 30.06.16. */
public class AbstractCmsStandardAttributeModifierTagProcessor
    extends AbstractStandardExpressionAttributeTagProcessor implements IAttributeDefinitionsAware {

  private final boolean removeIfEmpty;
  private final String targetAttrCompleteName;
  private AttributeDefinition targetAttributeDefinition;

  protected AbstractCmsStandardAttributeModifierTagProcessor(
      final TemplateMode templateMode,
      final String dialectPrefix,
      final String attrName,
      final int precedence,
      final boolean removeIfEmpty) {

    this(templateMode, dialectPrefix, attrName, attrName, precedence, removeIfEmpty);
  }

  protected AbstractCmsStandardAttributeModifierTagProcessor(
      final TemplateMode templateMode,
      final String dialectPrefix,
      final String attrName,
      final String targetAttrCompleteName,
      final int precedence,
      final boolean removeIfEmpty) {

    super(templateMode, dialectPrefix, attrName, precedence, false);

    Validate.notNull(targetAttrCompleteName, "Complete name of target attribute cannot be null");

    this.targetAttrCompleteName = targetAttrCompleteName;
    this.removeIfEmpty = removeIfEmpty;
  }

  public void setAttributeDefinitions(final AttributeDefinitions attributeDefinitions) {
    Validate.notNull(attributeDefinitions, "Attribute Definitions cannot be null");
    this.targetAttributeDefinition =
        attributeDefinitions.forName(getTemplateMode(), this.targetAttrCompleteName);
  }

  @Override
  protected final void doProcess(
      final ITemplateContext context,
      final IProcessableElementTag tag,
      final AttributeName attributeName,
      final String attributeValue,
      final Object expressionResult,
      final IElementTagStructureHandler structureHandler) {

    final String newAttributeValue =
        HtmlEscape.escapeHtml4Xml(expressionResult == null ? null : expressionResult.toString());
    if (this.removeIfEmpty && (newAttributeValue == null || newAttributeValue.length() == 0)) {
      structureHandler.removeAttribute(this.targetAttributeDefinition.getAttributeName());
      structureHandler.removeAttribute(attributeName);
    } else if (StringUtils.isNotBlank(newAttributeValue)) {
      StandardProcessorUtils.replaceAttribute(
          structureHandler,
          attributeName,
          this.targetAttributeDefinition,
          this.targetAttrCompleteName,
          newAttributeValue);
    }
  }
}
