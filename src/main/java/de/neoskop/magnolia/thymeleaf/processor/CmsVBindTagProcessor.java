package de.neoskop.magnolia.thymeleaf.processor;

import java.util.List;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.exceptions.TemplateProcessingException;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.expression.Assignation;
import org.thymeleaf.standard.expression.AssignationSequence;
import org.thymeleaf.standard.expression.AssignationUtils;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.NoOpToken;
import org.thymeleaf.standard.processor.StandardConditionalFixedValueTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.util.ArrayUtils;
import org.thymeleaf.util.EvaluationUtils;
import org.thymeleaf.util.StringUtils;
import org.unbescape.html.HtmlEscape;

/** Created by cbinzer on 05.07.16. */
public class CmsVBindTagProcessor extends AbstractAttributeTagProcessor {

  public static final int PRECEDENCE = 700;
  public static final String ATTR_NAME = "vbind";
  private final ModificationType modificationType = ModificationType.SUBSTITUTION;

  public CmsVBindTagProcessor(final String dialectPrefix) {
    super(TemplateMode.HTML, dialectPrefix, null, false, ATTR_NAME, true, PRECEDENCE, true);
  }

  @Override
  protected void doProcess(
      final ITemplateContext context,
      final IProcessableElementTag tag,
      final AttributeName attributeName,
      final String attributeValue,
      final IElementTagStructureHandler structureHandler) {

    final AssignationSequence assignations =
        AssignationUtils.parseAssignationSequence(context, attributeValue, false);
    if (assignations == null) {
      throw new TemplateProcessingException(
          "Could not parse value as attribute assignations: \"" + attributeValue + "\"");
    }

    final List<Assignation> assignationValues = assignations.getAssignations();
    final int assignationValuesLen = assignationValues.size();

    for (int i = 0; i < assignationValuesLen; i++) {
      final Assignation assignation = assignationValues.get(i);
      final IStandardExpression leftExpr = assignation.getLeft();
      final Object leftValue = leftExpr.execute(context);
      final IStandardExpression rightExpr = assignation.getRight();
      final Object rightValue = rightExpr.execute(context);

      if (rightValue == NoOpToken.VALUE) {
        continue;
      }

      String newAttributeName = (leftValue == null ? null : leftValue.toString());
      if (StringUtils.isEmptyOrWhitespace(newAttributeName)) {
        throw new TemplateProcessingException(
            "Attribute name expression evaluated as null or empty: \"" + leftExpr + "\"");
      }

      if (getTemplateMode() == TemplateMode.HTML
          && this.modificationType == ModificationType.SUBSTITUTION
          && ArrayUtils.contains(
              StandardConditionalFixedValueTagProcessor.ATTR_NAMES, newAttributeName)) {

        if (EvaluationUtils.evaluateAsBoolean(rightValue)) {
          newAttributeName = ":" + newAttributeName;
          structureHandler.setAttribute(newAttributeName, newAttributeName);
        } else {
          structureHandler.removeAttribute(newAttributeName);
        }
      } else {
        final String newAttributeValue =
            HtmlEscape.escapeHtml4Xml(rightValue == null ? null : rightValue.toString());
        if (newAttributeValue == null || newAttributeValue.length() == 0) {

          if (this.modificationType == ModificationType.SUBSTITUTION) {
            structureHandler.removeAttribute(newAttributeName);
          }
        } else {
          if (this.modificationType == ModificationType.SUBSTITUTION
              || !tag.hasAttribute(newAttributeName)
              || tag.getAttributeValue(newAttributeName).length() == 0) {
            newAttributeName = ":" + newAttributeName;
            structureHandler.setAttribute(newAttributeName, newAttributeValue);
          } else {
            String currentValue = tag.getAttributeValue(newAttributeName);
            newAttributeName = ":" + newAttributeName;
            if (this.modificationType == ModificationType.APPEND) {
              structureHandler.setAttribute(newAttributeName, currentValue + newAttributeValue);
            } else if (this.modificationType == ModificationType.APPEND_WITH_SPACE) {
              structureHandler.setAttribute(
                  newAttributeName, currentValue + ' ' + newAttributeValue);
            } else if (this.modificationType == ModificationType.PREPEND) {
              structureHandler.setAttribute(newAttributeName, newAttributeValue + currentValue);
            } else {
              structureHandler.setAttribute(
                  newAttributeName, newAttributeValue + ' ' + currentValue);
            }
          }
        }
      }
    }
  }

  protected enum ModificationType {
    SUBSTITUTION,
    APPEND,
    PREPEND,
    APPEND_WITH_SPACE,
    PREPEND_WITH_SPACE
  }
}
