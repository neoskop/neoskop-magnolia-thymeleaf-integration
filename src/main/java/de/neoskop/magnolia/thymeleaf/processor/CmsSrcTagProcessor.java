package de.neoskop.magnolia.thymeleaf.processor;

import org.thymeleaf.templatemode.TemplateMode;

/** Created by cbinzer on 30.06.16. */
public class CmsSrcTagProcessor extends AbstractCmsStandardAttributeModifierTagProcessor {

  public static final int PRECEDENCE = 1000;
  public static final String ATTR_NAME = "src";

  public CmsSrcTagProcessor(String dialectPrefix) {
    super(TemplateMode.HTML, dialectPrefix, ATTR_NAME, PRECEDENCE, false);
  }
}
