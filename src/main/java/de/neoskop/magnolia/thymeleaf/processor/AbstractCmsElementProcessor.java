package de.neoskop.magnolia.thymeleaf.processor;

import de.neoskop.magnolia.thymeleaf.dialect.MagnoliaDialect;
import info.magnolia.objectfactory.Components;
import info.magnolia.rendering.context.RenderingContext;
import info.magnolia.rendering.engine.RenderException;
import info.magnolia.templating.elements.TemplatingElement;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.exceptions.TemplateProcessingException;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.expression.Expression;
import org.thymeleaf.standard.expression.StandardExpressionParser;
import org.thymeleaf.templatemode.TemplateMode;

public abstract class AbstractCmsElementProcessor<T extends TemplatingElement>
    extends AbstractAttributeTagProcessor {

  public AbstractCmsElementProcessor(
      TemplateMode templateMode,
      String dialectPrefix,
      String elementName,
      boolean prefixElementName,
      String attributeName,
      boolean prefixAttributeName) {

    super(
        templateMode,
        dialectPrefix,
        elementName,
        prefixElementName,
        attributeName,
        prefixAttributeName,
        1000,
        true);
  }

  protected final T createElement(RenderingContext renderingContext) {
    return Components.getComponentProvider()
        .newInstance(getTemplatingElementClass(), renderingContext);
  }

  protected final Class<T> getTemplatingElementClass() {
    return (Class<T>)
        ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
  }

  protected void processElement(IElementTagStructureHandler structureHandler, T templatingElement) {
    StringBuilder out = new StringBuilder();

    try {
      templatingElement.begin(out);
      templatingElement.end(out);
    } catch (RenderException | IOException e) {
      throw new TemplateProcessingException("An error occurred on processing an cms element", e);
    }

    structureHandler.removeAllButFirstChild();
    structureHandler.replaceWith(out, false);
  }

  public boolean getEditable(IProcessableElementTag tag) {
    boolean editable = true;
    String editableAsString = tag.getAttributeValue(MagnoliaDialect.PREFIX, "editable");

    if (StringUtils.isNotBlank(editableAsString)) {
      editable = Boolean.valueOf(tag.getAttributeValue(MagnoliaDialect.PREFIX, "editable"));
    }

    return editable;
  }

  public Map<String, Object> getContextAttributes(
      ITemplateContext context, IProcessableElementTag tag) {
    Map<String, Object> contextAttributes = null;
    String contextAttributesAsString =
        tag.getAttributeValue(MagnoliaDialect.PREFIX, "contextAttributes");

    if (StringUtils.isNotBlank(contextAttributesAsString)) {
      Expression expression =
          new StandardExpressionParser().parseExpression(context, contextAttributesAsString);
      Object contextAttributesObject = expression.execute(context);

      if (contextAttributesObject != null) {
        if (contextAttributesObject instanceof Map<?, ?>) {
          contextAttributes = (Map<String, Object>) contextAttributesObject;
        } else {
          throw new TemplateProcessingException(
              "contextAttributes must be a Map. Passed value was a "
                  + contextAttributesObject.getClass().getSimpleName()
                  + ".");
        }
      }
    }

    return contextAttributes;
  }
}
