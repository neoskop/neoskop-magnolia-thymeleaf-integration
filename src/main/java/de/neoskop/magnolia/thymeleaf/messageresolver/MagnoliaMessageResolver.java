package de.neoskop.magnolia.thymeleaf.messageresolver;

import info.magnolia.i18nsystem.FixedLocaleProvider;
import info.magnolia.i18nsystem.TranslationService;
import info.magnolia.i18nsystem.util.MessageFormatterUtils;
import info.magnolia.objectfactory.Components;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.messageresolver.AbstractMessageResolver;
import org.thymeleaf.util.Validate;

/**
 * @author Arne Diekmann
 * @since 10.05.17
 */
public class MagnoliaMessageResolver extends AbstractMessageResolver {

  @Override
  public String resolveMessage(
      ITemplateContext context, Class<?> origin, String key, Object[] messageParameters) {
    Validate.notNull(context, "Context cannot be null");
    Validate.notNull(context.getLocale(), "Locale in context cannot be null");
    Validate.notNull(key, "Message key cannot be null");
    final FixedLocaleProvider localeProvider = new FixedLocaleProvider(context.getLocale());
    final String msg =
        Components.getComponent(TranslationService.class)
            .translate(localeProvider, new String[] {key});

    if (messageParameters.length > 0) {
      return MessageFormatterUtils.format(msg, localeProvider.getLocale(), messageParameters);
    } else {
      return msg;
    }
  }

  @Override
  public String createAbsentMessageRepresentation(
      ITemplateContext context, Class<?> origin, String key, Object[] messageParameters) {
    Validate.notNull(key, "Message key cannot be null");

    if (context.getLocale() != null) {
      return "??" + key + "_" + context.getLocale().toString() + "??";
    }

    return "??" + key + "_" + "??";
  }
}
